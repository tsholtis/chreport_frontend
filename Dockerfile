# base node image
FROM node:12.13.0

# set working directory
WORKDIR /app

# add node_modules to path
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json .
RUN npm install
RUN npm install --unsafe-perm -g @angular/cli@8.3.15

# install the app
COPY . .

# run the app
CMD ng serve --host 0.0.0.0 --port 4200
