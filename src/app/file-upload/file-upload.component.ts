import { Component, OnInit } from '@angular/core';

// custom imports
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { FileUploader } from "ng2-file-upload";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { MatFormFieldModule, MatSelectModule, MatCardModule, MatIconModule, MatInputModule } from "@angular/material";
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  uploadForm: FormGroup;

  public uploader: FileUploader = new FileUploader({
    isHTML5: true
  }); 

  title: string = 'Angular File Upload';
  reportlink: string;
  reporttitle: string;
  
  constructor(private fb: FormBuilder, private http: HttpClient) { }

  getServer() {
    return environment.serverHost;
  }

  getProtocol() {
    return environment.serverProtocol;
  }

  getPort() {
    return environment.serverPort;
  }

  getFileUploadPath() {
    return '/chreport/upload';
  }

  uploadSubmit() {
    // Check size of each file first before doing any further processing
    for (let i = 0; i < this.uploader.queue.length; i++) {
      let fileItem = this.uploader.queue[i]._file;
      if (fileItem.size > 1000000) {
        alert("Each file should be less than 10MB of size!");
        return;
      }
    }

    //  processing of uploads
    let data = new FormData();
    data.append('sourceType', this.uploadForm.controls.sourceType.value);
    data.append('reportName', this.uploadForm.controls.reportfilename.value);
    this.reporttitle = this.uploadForm.controls.reportfilename.value;

    for (let j = 0; j < this.uploader.queue.length; j++) {
      let fileItem = this.uploader.queue[j]._file;
      console.log("Processing file = " + fileItem.name);
      // this is strange - you'd think you'd build an array here and then append it but instead you create multiple
      //   'fileupload' mapped items in the FormData with append, then multer will pick all of them up on the server and make
      //    the array...
      data.append("fileupload", fileItem);   
    }
    this.uploadFile(data).subscribe(data => {
      console.log("data is = " + data);
      let protocol = this.getProtocol();
      let server = this.getServer();
      let port = this.getPort();
      this.reportlink = protocol+'://'+server+':'+port+data;
    }); 
    console.log("finished uploading data...");
    this.uploader.clearQueue();
    console.log("Queue cleared...");
  }

  uploadFile(data: FormData): Observable<any> {
    console.log("about to upload data...");
    let protocol = this.getProtocol();
    let server = this.getServer();
    let port = this.getPort();
    let uploadpath = this.getFileUploadPath();
    console.log("upload path is = " + protocol+'://'+server+':'+port+uploadpath);
    return this.http.post(protocol+'://'+server+':'+port+uploadpath, data, {
      responseType: 'text',
    });  
  }

  ngOnInit() {
    this.uploadForm = this.fb.group({
      document: [null, null],
      reportfilename: [null, Validators.compose([Validators.required])],
      sourceType: [null, Validators.compose([Validators.required])],
    });
  }
}
