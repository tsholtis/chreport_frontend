
export const environment = {
  production: true,
  serverHost: '<localserverhostdomain>',
  serverPort: 'process.env.SRV_PORT',
  serverProtocol: 'http',
};